package ru.kpfu.itis.site.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.kpfu.itis.framework.annotation.RequestMapping;
import ru.kpfu.itis.framework.annotation.RequestMethod;
import ru.kpfu.itis.site.dto.SignUpDto;
import ru.kpfu.itis.site.services.SecurityService;

import javax.servlet.http.HttpServletRequest;

@RequiredArgsConstructor
@Controller
public class AccountsController {

    @Autowired
    private final SecurityService securityService;

    @RequestMapping(path = "/signup")
    public String getSignUpPage() {
        return "signUp";
    }

    @RequestMapping(path = "/signup", method = RequestMethod.POST)
    public String saveAccount(HttpServletRequest request) {
        SignUpDto signUpDto = SignUpDto.builder()
            .firstName(request.getParameter("firstName"))
            .lastName(request.getParameter("lastName"))
            .email(request.getParameter("email"))
            .password(request.getParameter("password"))
            .build();
        securityService.signUp(signUpDto);
        return "signUp";
    }
}

