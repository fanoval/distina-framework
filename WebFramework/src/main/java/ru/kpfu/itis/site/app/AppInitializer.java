package ru.kpfu.itis.site.app;

import ru.kpfu.itis.framework.WebAppInitializer;
import ru.kpfu.itis.site.config.ApplicationConfig;

import javax.servlet.ServletContextEvent;
import javax.servlet.annotation.WebListener;

@WebListener
public class AppInitializer extends WebAppInitializer {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        registerConfigClass(sce, ApplicationConfig.class);
    }
}

