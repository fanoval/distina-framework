package ru.kpfu.itis.site.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.framework.templator.DefaultTemplateHandler;
import ru.kpfu.itis.framework.templator.TemplateHandler;

@Configuration
@ComponentScan({"ru.kpfu.itis.site.controllers", "ru.kpfu.itis.site.services"})
public class ApplicationConfig {

    @Bean
    public TemplateHandler templateHandler() {
        DefaultTemplateHandler templateHandler = new DefaultTemplateHandler();
        templateHandler.setBasePackagePath("/WEB-INF/jsp/");
        return templateHandler;
    }

//    @Bean
//    public TemplateHandler templateHandler() {
//        FreemarkerTemplateHandler templateHandler = new FreemarkerTemplateHandler("/freemarker/");
//        return templateHandler;
//    }
}

