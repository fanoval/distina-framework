package ru.kpfu.itis.site.controllers;

import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.kpfu.itis.framework.ModelAndView;
import ru.kpfu.itis.framework.annotation.RequestMapping;
import ru.kpfu.itis.framework.annotation.RequestMethod;
import ru.kpfu.itis.framework.exception.FileLoaderException;
import ru.kpfu.itis.framework.utils.FileLoader;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@Data
@MultipartConfig
public class TestController {

    @Autowired
    private FileLoader fileLoader;

    @RequestMapping(path = "/test")
    public ModelAndView getTestPage() {
        ModelAndView modelAndView = new ModelAndView("test");
        modelAndView.addAttribute("test", "123");
        return modelAndView;
    }

    @RequestMapping(path = "/test2")
    public String getTestPage2() {
        return "test";
    }

    @RequestMapping(path = "/test3")
    public void getTestPage3() {
        System.out.println("Окей");
    }

    @RequestMapping(path = "/test4")
    public String getTestPage4() {
        return null;
    }

    @RequestMapping(path = "/test5")
    public void getTestPage5(HttpServletResponse response, HttpServletRequest request) throws IOException {
        response.sendRedirect(request.getContextPath() + "/signup");
    }

    @RequestMapping(path = "/test6")
    public void getTestPage6(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setAttribute("test", "1234");
        request.getRequestDispatcher("/WEB-INF/jsp/test.jsp").forward(request, response);
    }

    @RequestMapping(path = "/test7")
    public void getTestPage7(HttpServletRequest request) throws IOException {
    }

    @RequestMapping(path = "/upload")
    public String getFileUploadPage() {
        return "fileUpload";
    }

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public String uploadFile(HttpServletRequest request) {
        fileLoader.upload("Ваш путь", request);
        return "fileUpload";
    }

    @RequestMapping(path = "/download")
    public void downLoadFile(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String fileName = request.getParameter("fileName");
        if (fileName != null) {
            try {
                fileLoader.download("Ваш путь", fileName, request, response);
            } catch (FileLoaderException e) {
                //обработать как-нибудь
            }
        } else {
            request.getRequestDispatcher("/WEB-INF/jsp/fileDownload.jsp").forward(request, response);
        }
    }
}

