package ru.kpfu.itis.framework.utils;

import lombok.Data;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.framework.exception.FileLoaderException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;

@Data
public class SimpleFileLoader implements FileLoader {

    private static final String CONTENT_DISPOSITION_HEADER = "Content-Disposition";

    @Autowired
    private ServletFileUpload servletFileUpload;

    @Override
    public void upload(String storagePath, HttpServletRequest request) {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {

            try {
                List items = servletFileUpload.parseRequest(request);
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();

                    if (!item.isFormField()) {
                        String fileName = item.getName();

                        File path = new File(storagePath);
                        if (!path.exists()) {
                            throw new FileLoaderException("Storage path doesn't exist");
                        }

                        File uploadedFile = new File(path + File.separator + fileName);
                        item.write(uploadedFile);
                    }
                }
            } catch (Exception e) {
                throw new FileLoaderException("Can't upload file", e);
            }
        } else {
            throw new FileLoaderException("It isn't a multipart content");
        }
    }

    @Override
    public void download(String storagePath, String fileName,
                         HttpServletRequest request, HttpServletResponse response) {
        response.setHeader(CONTENT_DISPOSITION_HEADER, "filename=" + fileName);

        try {
            Files.copy(Paths.get(storagePath + fileName), response.getOutputStream());
        } catch (IOException e) {
            throw new FileLoaderException("Can't read file", e);
        }
    }
}

