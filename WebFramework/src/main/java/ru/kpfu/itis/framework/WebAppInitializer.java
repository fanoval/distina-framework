package ru.kpfu.itis.framework;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static ru.kpfu.itis.framework.helper.Constants.CONFIG_CLASS_ATTRIBUTE_NAME;

public abstract class WebAppInitializer implements ServletContextListener {

    public void registerConfigClass(ServletContextEvent sce, Class<?> configClass) {
        sce.getServletContext().setAttribute(CONFIG_CLASS_ATTRIBUTE_NAME, configClass);
    }
}

