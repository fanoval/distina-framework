package ru.kpfu.itis.framework.handler;

import lombok.Data;
import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.framework.exception.MethodHandlerException;
import ru.kpfu.itis.framework.ModelAndView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

@Data
public class DefaultMethodHandler implements MethodHandler {

    private final InputParameterDefiner parameterDefiner;
    private final ReturnObjectHandler returnObjectHandler;

    @Override
    public ModelAndView handle(Method method, List<Object> possibleParameters, ApplicationContext context) {
        try {
            return returnObjectHandler.handle(method.invoke(context.getBean(method.getDeclaringClass()),
                    parameterDefiner.defineParameters(method, possibleParameters)));
        } catch (IllegalAccessException | InvocationTargetException e) {
            throw new MethodHandlerException(e);
        }
    }
}

