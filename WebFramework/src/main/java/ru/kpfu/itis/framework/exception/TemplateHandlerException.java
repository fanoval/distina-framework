package ru.kpfu.itis.framework.exception;

public class TemplateHandlerException extends RuntimeException{
    public TemplateHandlerException(String message) {
        super(message);
    }

    public TemplateHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public TemplateHandlerException(Throwable cause) {
        super(cause);
    }
}

