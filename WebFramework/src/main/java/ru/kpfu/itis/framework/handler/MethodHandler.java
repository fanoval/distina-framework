package ru.kpfu.itis.framework.handler;

import org.springframework.context.ApplicationContext;
import ru.kpfu.itis.framework.ModelAndView;

import java.lang.reflect.Method;
import java.util.List;

public interface MethodHandler {
    ModelAndView handle(Method method, List<Object> possibleParameters, ApplicationContext context);
}

