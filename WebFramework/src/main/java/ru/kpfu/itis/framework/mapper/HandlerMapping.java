package ru.kpfu.itis.framework.mapper;

import org.springframework.context.ApplicationContext;

import java.lang.reflect.Method;

public interface HandlerMapping {
    void register(ApplicationContext context);
    Method getHandler(String requestURI, String requestMethod);
}

