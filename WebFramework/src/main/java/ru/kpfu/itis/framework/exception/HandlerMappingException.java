package ru.kpfu.itis.framework.exception;

public class HandlerMappingException extends RuntimeException {
    public HandlerMappingException(String message) {
        super(message);
    }

    public HandlerMappingException(String message, Throwable cause) {
        super(message, cause);
    }

    public HandlerMappingException(Throwable cause) {
        super(cause);
    }
}

