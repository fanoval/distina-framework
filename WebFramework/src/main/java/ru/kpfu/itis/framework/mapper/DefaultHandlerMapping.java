package ru.kpfu.itis.framework.mapper;

import lombok.Data;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import ru.kpfu.itis.framework.annotation.RequestMapping;
import ru.kpfu.itis.framework.annotation.RequestMethod;
import ru.kpfu.itis.framework.exception.HandlerMappingException;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ru.kpfu.itis.framework.helper.BeanScanner.findClassesByAnnotation;

@Data
public class DefaultHandlerMapping implements HandlerMapping {

    private final static String REQUEST_URI_PATTERN = "^\\/[A-Za-z0-9-\\/]+$";

    private Map<String, HashMap<RequestMethod, Method>> requestMapping = new HashMap<>();

    @Override
    public void register(ApplicationContext context) {
        List<Class<?>> controllers = findClassesByAnnotation(context, Controller.class);
        for (Class<?> c: controllers) {

            Method[] methods = c.getMethods();
            for (Method m: methods) {

                if (m.isAnnotationPresent(RequestMapping.class)) {
                    RequestMapping annotation = m.getAnnotation(RequestMapping.class);

                    String requestURI = annotation.path();
                    if (validateRequestPattern(requestURI)) {
                        if (!requestMapping.containsKey(requestURI)) {
                            requestMapping.put(requestURI, new HashMap<>());
                        }
                        Map<RequestMethod, Method> methodMap = requestMapping.get(requestURI);
                        if (methodMap.containsKey(annotation.method())) {
                            throw new HandlerMappingException("You have the same mapping in " + m.getName());
                        } else {
                            methodMap.put(annotation.method(), m);
                        }
                    }
                    else {
                        throw new HandlerMappingException("Method " + m.getName() + " contains illegal request pattern.");
                    }
                }
            }
        }
    }

    @Override
    public Method getHandler(String requestURI, String requestMethod) {
        if (this.getRequestMapping().containsKey(requestURI)) {
            Map<RequestMethod, Method> methodMap = requestMapping.get(requestURI);
            if (methodMap.containsKey(RequestMethod.valueOf(requestMethod.toUpperCase()))) {
                Method method = methodMap.get(RequestMethod.valueOf(requestMethod.toUpperCase()));
                return method;
            }
        }
        throw new HandlerMappingException("Request path not found");
    }

    private boolean validateRequestPattern(String requestURI) {
        if (requestURI.matches(REQUEST_URI_PATTERN)) {
            return true;
        }
        return false;
    }
}

