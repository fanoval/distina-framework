package ru.kpfu.itis.framework.handler;

import ru.kpfu.itis.framework.ModelAndView;

public interface ReturnObjectHandler {
    ModelAndView handle(Object result);
}

