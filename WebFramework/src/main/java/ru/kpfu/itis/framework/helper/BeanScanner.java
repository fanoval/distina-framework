package ru.kpfu.itis.framework.helper;

import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

public class BeanScanner {

    public static List<Class<?>> findClassesByAnnotation(ApplicationContext context, Class annotationClass) {
        List<Class<?>> list = new ArrayList<>();
        for (Object bean: context.getBeansWithAnnotation(annotationClass).values()) {
            list.add(bean.getClass());
        }
        return list;
    }
}

