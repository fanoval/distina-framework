package ru.kpfu.itis.framework.exception;

public class InputParameterDefinerException extends RuntimeException{
    public InputParameterDefinerException(String message) {
        super(message);
    }

    public InputParameterDefinerException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputParameterDefinerException(Throwable cause) {
        super(cause);
    }
}

