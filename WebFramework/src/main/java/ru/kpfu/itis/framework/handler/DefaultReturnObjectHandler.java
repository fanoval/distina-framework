package ru.kpfu.itis.framework.handler;

import ru.kpfu.itis.framework.ModelAndView;
import ru.kpfu.itis.framework.exception.ReturnObjectHandlerException;

public class DefaultReturnObjectHandler implements ReturnObjectHandler {

    @Override
    public ModelAndView handle(Object result) {
        if (result == null) {
            return null;
        }
        if (result instanceof String) {
            return new ModelAndView((String) result);
        }
        if (result instanceof ModelAndView) {
            return (ModelAndView) result;
        }
        throw new ReturnObjectHandlerException("Invalid return data type.");
    }
}

