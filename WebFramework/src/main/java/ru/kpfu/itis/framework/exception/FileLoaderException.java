package ru.kpfu.itis.framework.exception;

public class FileLoaderException extends RuntimeException {
    public FileLoaderException(String message) {
        super(message);
    }

    public FileLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    public FileLoaderException(Throwable cause) {
        super(cause);
    }
}

