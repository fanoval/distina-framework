package ru.kpfu.itis.framework.exception;

public class ReturnObjectHandlerException extends RuntimeException{
    public ReturnObjectHandlerException(String message) {
        super(message);
    }

    public ReturnObjectHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReturnObjectHandlerException(Throwable cause) {
        super(cause);
    }
}

