package ru.kpfu.itis.framework.exception;

public class MainServletInitializeException extends RuntimeException {

    public MainServletInitializeException(String message) {
        super(message);
    }

    public MainServletInitializeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MainServletInitializeException(Throwable cause) {
        super(cause);
    }
}

