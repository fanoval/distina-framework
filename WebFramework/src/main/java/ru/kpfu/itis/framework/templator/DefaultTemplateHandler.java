package ru.kpfu.itis.framework.templator;

import lombok.Data;
import ru.kpfu.itis.framework.exception.TemplateHandlerException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Data
public class DefaultTemplateHandler implements TemplateHandler {

    private static final String DEFAULT_SUFFIX = ".jsp";

    private String basePackagePath = "";
    String suffix = "";

    public DefaultTemplateHandler() {
        setSuffix(DEFAULT_SUFFIX);
    }

    @Override
    public void render(String viewName, Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        if (model != null) {
            for (Map.Entry<String, Object> atr : model.entrySet()) {
                request.setAttribute(atr.getKey(), atr.getValue());
            }
        }
        try {
            request.getServletContext()
                    .getRequestDispatcher(basePackagePath + viewName + suffix).forward(request, response);
        } catch (ServletException | IOException e) {
            throw new TemplateHandlerException("Can't process template", e);
        }
    }
}

