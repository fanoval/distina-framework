package ru.kpfu.itis.framework.utils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface FileLoader {
    void upload(String storagePath, HttpServletRequest request);
    void download(String storagePath, String parameterFileName, HttpServletRequest request, HttpServletResponse response);
}

