package ru.kpfu.itis.framework.handler;

import ru.kpfu.itis.framework.exception.InputParameterDefinerException;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class DefaultInputParameterDefiner implements InputParameterDefiner {
    @Override
    public Object[] defineParameters(Method method, List<Object> objects) {
            List<Object> list = new ArrayList<>();
            Class<?>[] neededParameters = method.getParameterTypes();
            boolean used = false;
            for (Class p: neededParameters) {
                for (Object o:objects) {
                    if (p.isInstance(o) && !list.contains(o)) {
                        list.add(o);
                        used = true;
                    }
                }
                if (!used) {
                    throw new InputParameterDefinerException("Invalid requested parameters");
                }
                used = false;
            }
            return list.toArray();
    }
}

