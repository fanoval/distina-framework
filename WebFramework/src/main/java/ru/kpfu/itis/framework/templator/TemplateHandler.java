package ru.kpfu.itis.framework.templator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface TemplateHandler {
    void render(String viewName, Map<String, Object> model, HttpServletRequest request, HttpServletResponse response);
}

