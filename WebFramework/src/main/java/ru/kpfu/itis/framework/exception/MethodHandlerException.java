package ru.kpfu.itis.framework.exception;

public class MethodHandlerException extends RuntimeException {
    public MethodHandlerException(String message) {
        super(message);
    }

    public MethodHandlerException(String message, Throwable cause) {
        super(message, cause);
    }

    public MethodHandlerException(Throwable cause) {
        super(cause);
    }
}

