package ru.kpfu.itis.framework;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.kpfu.itis.framework.config.ServiceConfiguration;
import ru.kpfu.itis.framework.exception.MainServletInitializeException;
import ru.kpfu.itis.framework.exception.HandlerMappingException;
import ru.kpfu.itis.framework.handler.MethodHandler;
import ru.kpfu.itis.framework.mapper.DefaultHandlerMapping;
import ru.kpfu.itis.framework.templator.TemplateHandler;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static ru.kpfu.itis.framework.helper.Constants.CONFIG_CLASS_ATTRIBUTE_NAME;

@WebServlet("/")
public class MainServlet extends HttpServlet {

    private static final Class SERVICE_CONFIG_CLASS = ServiceConfiguration.class;

    private ApplicationContext context;

    private DefaultHandlerMapping handlerMapping;
    private TemplateHandler templateHandler;
    private MethodHandler methodHandler;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        createContext(config.getServletContext());
        initTemplateHandler(context);
        initHandlerMapping(context);
        initMethodHandler(context);
    }

    @Override
    protected void service(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String requestURI = request.getRequestURI();
        String cleanRequestURI = requestURI.substring(request.getContextPath().length());

        Method method = null;
        try {
            method = handlerMapping.getHandler(cleanRequestURI, request.getMethod());
        } catch (HandlerMappingException e) {
            response.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        ModelAndView mav = this.methodHandler.handle(method, createPossibleMethodParametersList(request, response), context);

        if (mav != null) {
            this.templateHandler.render(mav.getViewName(), mav.getModel(), request, response);
        }
    }

    private void initTemplateHandler(ApplicationContext context) {
        try {
            this.templateHandler = context.getBean(TemplateHandler.class);
        } catch (NoSuchBeanDefinitionException e) {
            throw new MainServletInitializeException("Define template handler bean", e);
        }
    }

    private void initHandlerMapping(ApplicationContext context) {
        try {
            this.handlerMapping = context.getBean(DefaultHandlerMapping.class);
        } catch (NoSuchBeanDefinitionException e) {
            //ignore because it was added in service config file
        }
        handlerMapping.register(context);
    }

    private void initMethodHandler(ApplicationContext context) {
        try {
            this.methodHandler = context.getBean(MethodHandler.class);
        } catch (NoSuchBeanDefinitionException e) {
            //ignore because it was added in service config file
        }
    }

    private void createContext(ServletContext servletContext) {
        Class configClass = (Class) servletContext.getAttribute(CONFIG_CLASS_ATTRIBUTE_NAME);
        if (configClass != null) {
            this.context = new AnnotationConfigApplicationContext(SERVICE_CONFIG_CLASS, configClass);
        } else {
            throw new MainServletInitializeException("you didn't register configuration class.");
        }
    }

    private List<Object> createPossibleMethodParametersList(Object ... objects) {
        List<Object> list = new ArrayList<>();
        for (Object o: objects) {
            list.add(o);
        }
        return list;
    }
}

