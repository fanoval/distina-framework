package ru.kpfu.itis.framework.config;

import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.kpfu.itis.framework.handler.*;
import ru.kpfu.itis.framework.mapper.HandlerMapping;
import ru.kpfu.itis.framework.mapper.DefaultHandlerMapping;
import ru.kpfu.itis.framework.utils.FileLoader;
import ru.kpfu.itis.framework.utils.SimpleFileLoader;

@Configuration
public class ServiceConfiguration {

    @Bean
    public HandlerMapping requestMapper() {
        return new DefaultHandlerMapping();
    }

    @Bean
    public MethodHandler methodHandler() {
        return new DefaultMethodHandler(parameterDefiner(), returnObjectHandler());
    }

    @Bean
    public InputParameterDefiner parameterDefiner() {
        return new DefaultInputParameterDefiner();
    }

    @Bean
    public ReturnObjectHandler returnObjectHandler() {
        return new DefaultReturnObjectHandler();
    }

    @Bean
    public FileLoader fileLoader() {
        return new SimpleFileLoader();
    }

    @Bean
    public ServletFileUpload servletFileUpload() {
        return new ServletFileUpload(fileItemFactory());
    }

    @Bean
    public FileItemFactory fileItemFactory() {
        return new DiskFileItemFactory();
    }
}

