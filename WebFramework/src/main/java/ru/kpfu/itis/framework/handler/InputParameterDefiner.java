package ru.kpfu.itis.framework.handler;

import java.lang.reflect.Method;
import java.util.List;

public interface InputParameterDefiner {
    Object[] defineParameters(Method method, List<Object> objects);
}

