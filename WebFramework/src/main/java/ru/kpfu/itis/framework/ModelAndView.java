package ru.kpfu.itis.framework;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ModelAndView {

    private String viewName;
    private Map<String, Object> model;

    public ModelAndView(String viewName) {
        this(viewName, new HashMap<>());
    }

    public void addAttribute(String key, Object value) {
        this.model.put(key, value);
    }
}

