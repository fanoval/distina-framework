package ru.kpfu.itis.framework.templator.freemarker;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.Data;
import ru.kpfu.itis.framework.templator.TemplateHandler;
import ru.kpfu.itis.framework.exception.TemplateHandlerException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@Data
public class FreemarkerTemplateHandler implements TemplateHandler {

    private static final String DEFAULT_SUFFIX = ".ftlh";
    private static final String DEFAULT_ENCODING = "UTF-8";

    private Configuration configuration;
    private String suffix = "";

    public FreemarkerTemplateHandler(String basePackagePath) {
        setSuffix(DEFAULT_SUFFIX);
        this.configuration = new Configuration(Configuration.VERSION_2_3_29);
        this.configuration.setClassForTemplateLoading(this.getClass(), basePackagePath);
        setDefaultEncoding(DEFAULT_ENCODING);
    }

    @Override
    public void render(String viewName, Map<String, Object> model, HttpServletRequest request, HttpServletResponse response) {
        try {
            configuration.getTemplate(viewName + suffix)
                    .process(model, response.getWriter());
        } catch (TemplateException | IOException e) {
            throw new TemplateHandlerException("Can't process template.", e);
        }
    }

    public void setDefaultEncoding(String defaultEncoding) {
        this.configuration.setDefaultEncoding(defaultEncoding);
    }
}

