<%--
  Created by IntelliJ IDEA.
  User: r077r
  Date: 25.02.2022
  Time: 21:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="UTF-8">
    <title>Registration</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div style="background-color: rgba(0, 0, 0, 0.05);">
    <div class="row d-flex justify-content-center align-items-center">
        <div class="col-lg-8 col-xl-6">
            <div class="card rounded-3">
                <div class="card-body p-4 p-md-5">
                    <h3 class="mb-4 pb-2 pb-md-0 mb-md-5 px-md-2">Registration</h3>

                    <c:if test = "${not empty error}">
                        <p style="color: #e75b09; font-weight: bold;">${error}</p>
                    </c:if>

                    <form action="<c:url value="signup"/>" method="POST">

                        <div class="form-outline mb-4">
                            <input type="text" name="firstName" placeholder="First Name" class="form-control name field" />
                        </div>

                        <div class="form-outline mb-4">
                            <input type="text" name="lastName" placeholder="Last Name" class="form-control name field" />
                        </div>

                        <div class="form-outline mb-4">
                            <input type="email" name="email" placeholder="Email" class="form-control field email" />
                        </div>

                        <div class="form-outline mb-4">
                            <input type="password" name="password" placeholder="Password" class="form-control field email" />
                        </div>

                        <button type='submit' class="btn" style="background: #D2B48C">Registration</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
