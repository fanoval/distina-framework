<%--
  Created by IntelliJ IDEA.
  User: r077r
  Date: 15.03.2022
  Time: 18:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Upload files</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="contents" align="center">
    <form action="<c:url value="upload"/>" method="POST" enctype="multipart/form-data">
        <br>
        <input type="file" name="fileName" placeholder="Enter name of file..." multiple>
        <br>
        <br>
        <input type="submit" value="Send">
    </form>
</div>
</body>
</html>


